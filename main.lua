require 'torch'
require 'paths'
require 'xlua'
require 'optim'
require 'nn'

torch.setdefaulttensortype('torch.FloatTensor')

local opts = paths.dofile('opts.lua')

opt = opts.parse(arg)

nClasses = opt.nClasses

if opt.cuda then
    require 'cutorch'
    require 'cunn'
    cutorch.setDevice(1) -- by default, use GPU 1
end

paths.dofile('model.lua')

print(opt)


torch.manualSeed(opt.manualSeed)

print('Saving everything to: ' .. opt.save)
os.execute('mkdir -p ' .. opt.save)

if opt.loaddata then
    trainset = torch.load('training_data.net')
    validationset = torch.load('validation_data.net')
else
    paths.dofile('datareader.lua')
end

paths.dofile('train.lua')
paths.dofile('test.lua')

print("Starting Training")
local last_accuracy = 0
local decreasing = 0
local threshold = 1 -- number of deacreasing epochs allowed
epoch = opt.epochNumber

for i=1,opt.nEpochs do
    print(os.date())
    local loss = step()
    if opt.cuda then cutorch.synchronize() end
    print(string.format('Epoch: %d Current loss: %4f', i, loss))
    print(os.date())
    local accuracy = eval(validationset, opt.batchSize)
    if opt.cuda then cutorch.synchronize() end
    print(string.format('Accuracy on the validation set: %4f', accuracy))
    if accuracy < last_accuracy then
        if decreasing > threshold then break end
        decreasing = decreasing + 1
    else
        decreasing = 0
    end
    last_accuracy = accuracy

    epoch = epoch + 1
end

filename = paths.concat(opt.save, 'modelretail.net')
torch.save(filename, model)
