require 'nn'
require 'optim'
require 'cutorch'
reader = require 'reader'

fullset = reader.fullset()

trainset = {
    size = 1900000,
    data = {},
    labels = fullset.labels[{{1,1900000}}]
}

validationset = {
    size = 100000,
    data = {},
    labels = fullset.labels[{{1900001, 2000000}}]
}

for i =1, 2000000 do
    if (i <= 1900000) then
        trainset.data[i] = fullset.data[i]
    else
        validationset.data[i-1900000] = fullset.data[i]
    end
end


model = nn.Sequential()
model:add(nn.SparseLinear(5013, 1000, true))
model:add(nn.Tanh())
model:add(nn.Linear(1000, 200))
model:add(nn.Tanh())
model:add(nn.Linear(200, 30))
model:add(nn.Tanh())
model:add(nn.Linear(30, 8))
model:add(nn.LogSoftMax())
-- model:cuda()

criterion = nn.ClassNLLCriterion()

sgd_params = {
   learningRate = 1e-2,
   learningRateDecay = 1e-4,
   weightDecay = 1e-3,
   momentum = 1e-4
}

x, dl_dx = model:getParameters()

function step(batch_size)
    local current_loss = 0
    local count = 0
    batch_size = batch_size or 4000

    for t = 1, trainset.size, batch_size do
        -- setup inputs and targets for this mini-batch
        print(t, t+batch_size)
        local size = math.min(t + batch_size - 1, trainset.size) - t
        local targets = torch.Tensor(size)
        local inputs = {}
        for i = 1, size do
            inputs[i] = trainset.data[t+i]
            targets[i] = trainset.labels[t+i]
        end

        local feval = function(x_new)
            -- reset data
            if x ~= x_new then x:copy(x_new) end
            dl_dx:zero()

            -- perform mini-batch gradient descent
            local loss = criterion:forward(model:forward(inputs), targets)
            model:backward(inputs, criterion:backward(model.output, targets))

            return loss, dl_dx
        end

        _, fs = optim.sgd(feval, x, sgd_params)
        -- fs is a table containing value of the loss function
        count = count + 1
        current_loss = current_loss + fs[1]
        print(current_loss)
    end
    -- normalize loss
    return current_loss / count
end

function eval(dataset, batch_size)
    local count = 0
    batch_size = batch_size or 4000

    for i = 1, dataset.size, batch_size do
        local size = math.min(i + batch_size - 1, dataset.size) - i
        local inputs = {unpack(dataset.data, i, i+size-1)}
        local targets = dataset.labels[{{i,i+size-1}}]:long()
        local outputs = model:forward(inputs)
        local _, indices = torch.max(outputs, 2)
        local guessed_right = indices:eq(targets):sum()
        count = count + guessed_right
    end

    return count / dataset.size
end

max_iters = 30

do
    print("Starting Training")
    local last_accuracy = 0
    local decreasing = 0
    local threshold = 1 -- number of deacreasing epochs allowed
    for i = 1,max_iters do
        local loss = step()
        print(string.format('Epoch: %d Current loss: %4f', i, loss))
        local accuracy = eval(validationset)
        print(string.format('Accuracy on the validation set: %4f', accuracy))
        if accuracy < last_accuracy then
            if decreasing > threshold then break end
            decreasing = decreasing + 1
        else
            decreasing = 0
        end
        last_accuracy = accuracy
    end
end

-- -- test on test set
-- test_accuracy = eval(newtestset)
-- print(string.format('Accuracy on the test set: %4f', test_accuracy))

-- save model
paths = require 'paths'
filename = paths.concat(paths.cwd(), 'modelretail.net')
torch.save(filename, model)



