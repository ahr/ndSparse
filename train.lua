sgd_params = {
   learningRate = opt.LR,
   learningRateDecay = 1e-4,
   weightDecay = opt.weightDecay,
   momentum = opt.momentum
}

-- size = math.min(t + opt.batchSize - 1, trainset.size) - t

targets = torch.Tensor(opt.batchSize)
if opt.cuda then
    targets = targets:cuda()
    input_tensor = torch.CudaTensor(opt.batchSize, 5013)
end

x, dl_dx = model:getParameters()
function step()
    local current_loss = 0
    local count = 0

    for t = 1, trainset.size, opt.batchSize do
        -- setup inputs and targets for this mini-batch
        -- print(t, t+batch_size)

        local inputs = {}
        for i = 1, opt.batchSize do
            inputs[i] = trainset.data[t+i-1]
        end
        targets = targets:copy(trainset.labels[{{t, t+opt.batchSize - 1}}])
        if opt.linear then
            inputs = toDenseTensor(inputs, 5013)
            if opt.cuda then
                input_tensor = input_tensor:copy(inputs)
            end
        end

        local feval = function(x_new)
            -- reset data
            if x ~= x_new then x:copy(x_new) end
            dl_dx:zero()

            -- perform mini-batch gradient descent
            if opt.cuda then
                loss = criterion:forward(model:forward(input_tensor), targets)
                model:backward(input_tensor, criterion:backward(model.output, targets))
            else
                loss = criterion:forward(model:forward(inputs), targets)
                model:backward(inputs, criterion:backward(model.output, targets))
            end

            return loss, dl_dx
        end

        _, fs = optim.sgd(feval, x, sgd_params)
        -- fs is a table containing value of the loss function
        count = count + 1
        current_loss = current_loss + fs[1]
        -- print(current_loss)
    end
    -- normalize loss
    return current_loss / count
end
