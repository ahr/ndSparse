

function tosparse(input)
    local output = {}

    local dims = input:dim()
    local shape = input:size()
    local nonzeros = input:ne(0):sum()
    local values = torch.DoubleTensor(nonzeros):zero()
    local indices = torch.IntTensor(nonzeros, dims):zero()

    local counter = torch.ones(dims)

    local indexval = 1

    -- generate indices and values
    local function sparse(d, inp)
        if inp:dim() == 1 then
            for i = 1, inp:size(1) do
                if inp[i] ~= 0 then
                    counter[-1] = i
                    indices[indexval] = counter
                    values[indexval] = inp[i]
                    indexval = indexval + 1
                end
            end
        else
            for j = 1, inp:size(1) do
                 counter[d] = j
                 sparse(d+1, inp[j])
            end
        end
    end

    sparse(1, input)
    output['shape'] = shape
    output['values'] = values
    output['indices'] = indices
    return output

end


function todense(input)
    output = torch.Tensor(input.shape):zero()
    for i = 1, input.values:size(1) do
        output[torch.totable(input.indices[i])] = input.values[i]
    end
    return output
end

function toSparseTable(input)

    local output = {}

    if torch.isTensor(input) then
    -- from a sparse tensor (only 2D)
        -- local dims = input:dim()

        -- local counter = torch.ones(dims)

        -- generate indices and values
        local function sparse(d, inp)

            if inp:dim() == 1 then
                local indexval = 1

                local nonzeros = inp:ne(0):sum()
                local temp = torch.Tensor(nonzeros, 2):zero()

                for i = 1, inp:size(1) do
                    if inp[i] ~= 0 then
                        -- counter[-1] = i
                        temp[indexval] = torch.Tensor({i, inp[i]})
                        indexval = indexval + 1
                    end
                end
                return temp
            elseif inp:dim() == 2 then
                for j = 1, inp:size(1) do
                     output[j] = sparse(d+1, inp[j])
                end
            else
                error("Cannot process more than 2 dimensions right now")
            end
        end

        out = sparse(1, input)
        if input:dim() == 1 then return out end
    end
    if torch.type(input) == "table" then
        -- from sparse object (only for 2D)
        if input.indices:dim() == 2 then
            local rowcounter = 1
            for i = 1, input.shape[1] do
                local nonzeros = input.indices:narrow(2, 1, 1):eq(i):sum()
                if nonzeros ~= 0 then
                    local temp = torch.Tensor(nonzeros, 2):zero()
                    temp[{{}, {1}}] = input.indices:narrow(1, rowcounter, nonzeros)[{{}, {2}}]
                    temp[{{}, {2}}] = input.values:narrow(1, rowcounter, nonzeros)
                    output[i] = temp
                    rowcounter = rowcounter + nonzeros
                else
                    local temp = torch.Tensor(nonzeros, 0)
                    output[i] = temp
                end
            end
        else
            error("Cannot convert sparse objects that are not 2D")
        end
    end
    return output

end

-- test code

-- x1 = torch.Tensor({{1, 0, 0, 0, 0, 1, 1},
--                   {0, 0, 0, 0, 0, 0, 0},
--                   {0, 1, 0, 0, 1, 0, 0}})
-- x2 = torch.Tensor({0, 0, 0, 1, 0, 1, 0})

-- slam = torch.LongStorage(3)
-- slam[1] = 3
-- slam[2] = 3
-- slam[3] = 3
-- x3 = torch.Tensor(slam):zero()
-- x3[1][1][1] = 1
-- x3[2][2][2] = 1
-- x3[3][2][3] = 1

-- y = toSparseTable(x1)


-- print(y.values)
-- print(y.shape)
-- print(y.indices)
-- print(y)
-- z = toSparseTable(y)
-- print(z)
-- print(z[1])
-- print(z[3])
