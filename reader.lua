local reader = {}

filename_train = '../data/IMTrain'
filename_test = '../data/IMTest'

FULLSET_SIZE = 100
TESTSET_SIZE = 72000


local function ascread(fname, size)
    print('Reading ' .. fname)
    local function readline(line)
        local label = tonumber(string.match(line,'^([%+%-]?%s?%d+)'))
        if not label then
            error('could not read label')
        end
        -- label can be anything
        -- if label ~= 1 and label ~=-1 then
        --  error('label has to be +1 or -1')
        -- end
        local vals = {}
        local inds = {}
        local indcntr = 0
        for ind,val in string.gmatch(line,'(%d+):([%+%-]?%d?%.?%d+)') do
            indcntr = indcntr + 1
            ind = tonumber(ind)
            val = tonumber(val)
            if not ind or not val then
                error('reading failed')
            end
            if ind < indcntr then
                error('indices are not in increasing order')
            end
            table.insert(inds,ind)
            table.insert(vals,val)
        end
        return label,{torch.IntTensor(inds),torch.FloatTensor(vals)}
    end
    local data = {}
    local maxdim = 0
    local npos = 0
    local nneg = 0
    local minsparse = math.huge
    local maxsparse = 0
    local counter = 1
    local countmax = size or 0
    for line in io.lines(fname) do
        local lbl,vals = readline(line)
        table.insert(data,{lbl,vals})
        -- stats
        maxdim = math.max(maxdim,vals[1][-1])
        if lbl == 1 then npos = npos + 1 else nneg = nneg + 1 end
        minsparse = math.min(minsparse,vals[1]:size(1))
        maxsparse = math.max(maxsparse,vals[1]:size(1))
        if counter == countmax then
            break
        end
        counter = counter + 1
    end
    io.write(string.format("# of positive samples = %d\n",npos))
    io.write(string.format("# of negative samples = %d\n",nneg))
    io.write(string.format("# of total    samples = %d\n",#data))
    io.write(string.format("# of max dimensions   = %d\n",maxdim))
    io.write(string.format("Min # of dims = %d\n",minsparse))
    io.write(string.format("Max # of dims = %d\n",maxsparse))
    return data,maxdim
end


local function toSparseTable(input, size)

    local output = {}
            local rowcounter = 1
            for i = 1, size do
                local nonzeros = input[i][2][1]:size()[1]
                if nonzeros ~= nil then
                    local temp = torch.Tensor(nonzeros, 2):zero()
                    temp[{{}, {1}}] = input[i][2][1]
                    temp[{{}, {2}}] = input[i][2][2]
                    output[i] = temp
                else
                    local temp = torch.Tensor(nonzeros, 0)
                    output[i] = temp
                end
            end
    return output

end


function reader.fullset()
    rawdata = ascread(filename_train, FULLSET_SIZE)
    local fullset = {
        size = FULLSET_SIZE
    }
    local labels = torch.IntTensor(fullset.size):zero()
    -- get labels
    for i = 1, fullset.size do
        labels[i] = rawdata[i][1]
    end
    fullset['labels'] = labels

    -- get data
    fullset['data'] = toSparseTable(rawdata, fullset.size)

    return fullset
end


function reader.testset()
    local rawdata = ascread(filename_test)
    local testset = {
        size = TESTSET_SIZE
    }
    local labels = torch.IntTensor(testset.size):zero()

    -- get labels
    for i = 1, testset.size do
        labels[i] = rawdata[i][1]
    end
    testset['labels'] = labels

    -- get data
    testset['data'] = toSparseTable(rawdata, testset.size)

    return testset

end

return reader
