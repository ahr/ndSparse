require 'nn'
require 'src.Sparsed'
mnist = require 'mnist'

testset = mnist.testdataset()
testset.reshaped = torch.DoubleTensor(testset.size, 28*28)
testset.sparsed = {}
testset.reshaped = nn.Reshape(28*28):forward(testset.data:double())
testset.sparsed = toSparseTable(testset.reshaped)

eval = function(dataset, sparse, model)
    local count = 0
    for i = 1,dataset.size do
        if sparse == true then
            output = model:forward(dataset.sparsed[i])
        else
            output = model:forward(dataset.reshaped[i])
        end
        local _, index = torch.max(output, 1) -- max index
        index:add(-1)
        local digit = index[1] % 10
        if digit == dataset.label[i] then count = count + 1 end
    end

    return count / dataset.size
end


-- -- sparsed
filename = 'modelsparse.net'
model1 = torch.load(filename)

test_accuracy_sparsed = eval(testset, true, model1)
print(string.format('Accuracy using Sparse Layer on the test set: %4f', test_accuracy_sparsed))


-- linear
filename = 'modellinear.net'
model2 = torch.load(filename)

test_accuracy_linear = eval(testset, false, model2)
print(string.format('Accuracy using linear layer on the test set: %4f', test_accuracy_linear))
