filename_train = opt.data .. 'IMTrain'
filename_test = opt.data .. 'IMTest'

-- MAX_TABLE_SIZE = 1000000
-- READ_FLAG = true


function toSparseTable(input)
    local nonzeros = input[1]:size()[1]
    if nonzeros ~= nil then
        -- if opt.cuda then
            -- temp = torch.CudaTensor(nonzeros, 2):zero()
        -- else
        temp = torch.Tensor(nonzeros, 2):zero()
        -- end
        temp[{{}, {1}}] = input[1]
        temp[{{}, {2}}] = input[2]
    else
        local temp = torch.Tensor(nonzeros, 0)
    end
    return temp
end

function toDenseTensor(input, ndims)
    -- if opt.cuda then
        -- temp = torch.CudaTensor(#input, ndims):zero()
    -- else
        temp = torch.Tensor(#input, ndims):zero()
    -- end
    for i = 1, #input do
        sparse = input[i]
        size = #sparse
        for j = 1, size[1] do
            temp[i][sparse[j][1]] = sparse[j][2]
        end
    end
    return temp
end

local function ascread(fname, size)
    local function readline(line)
        local label = tonumber(string.match(line,'^([%+%-]?%s?%d+)'))
        if not label then
            error('could not read label')
        end
        -- label can be anything
        -- if label ~= 1 and label ~=-1 then
        --  error('label has to be +1 or -1')
        -- end
        local vals = {}
        local inds = {}
        local indcntr = 0
        for ind,val in string.gmatch(line,'(%d+):([%+%-]?%d?%.?%d+)') do
            indcntr = indcntr + 1
            ind = tonumber(ind)
            val = tonumber(val)
            if not ind or not val then
                error('reading failed')
            end
            if ind < indcntr then
                error('indices are not in increasing order')
            end
            table.insert(inds,ind)
            table.insert(vals,val)
        end
        return label,toSparseTable({torch.IntTensor(inds),torch.FloatTensor(vals)})
    end
    local data = {}
    local labels = {}
    local maxdim = 0
    local npos = 0
    local nneg = 0
    local minsparse = math.huge
    local maxsparse = 0
    local counter = 1
    local countmax = size or 0
    -- for i = 1, 5 do  -- stress test loop
        print('Reading ' .. fname)
        for line in io.lines(fname) do
            local lbl,vals = readline(line)
            table.insert(labels, lbl)
            table.insert(data, vals)
            -- stats
            maxdim = math.max(maxdim, vals[-1][1])
            if lbl == 1 then npos = npos + 1 else nneg = nneg + 1 end
            minsparse = math.min(minsparse,vals[1]:size(1))
            maxsparse = math.max(maxsparse,vals[1]:size(1))
            if counter == countmax then
                break
            end
            counter = counter + 1
        end
    -- end
    io.write(string.format("# of positive samples = %d\n",npos))
    io.write(string.format("# of negative samples = %d\n",nneg))
    io.write(string.format("# of total    samples = %d\n",#data))
    io.write(string.format("# of max dimensions   = %d\n",maxdim))
    io.write(string.format("Min # of dims = %d\n",minsparse))
    io.write(string.format("Max # of dims = %d\n",maxsparse))
    return torch.IntTensor(labels), data, maxdim
end


function fullset()
    if opt.nTrain ~= 0 then
        FULLSET_SIZE = opt.nTrain
        labels, data, _ = ascread(filename_train, FULLSET_SIZE)

    else
        labels, data, _ = ascread(filename_train)
    end
    local fullset = {
                        size = #data,
                        data = data,
                        labels = labels
                    }
    collectgarbage()
    return fullset
end


function testset()
    labels, data, _ = ascread(filename_train)
    local testset = {
                        size = #data,
                        data = data,
                        labels = labels
                    }
    collectgarbage()
    return testset
end

fullset = fullset()
trainset = {
    size = 0.8*fullset.size,
    data = {},
    labels = fullset.labels[{{1,0.8*fullset.size}}]
}

validationset = {
    size = 0.2*fullset.size,
    data = {},
    labels = fullset.labels[{{0.8*fullset.size+1, fullset.size}}]
}

for i = 1, fullset.size do
    if (i <= trainset.size) then
        trainset.data[i] = fullset.data[i]
    else
        validationset.data[i-trainset.size] = fullset.data[i]
    end
end

print('Done Reading Data')
print('Fullset Size ' .. #fullset.data)
print('Training set Size ' .. #trainset.data)
print('Validation set Size ' .. #validationset.data)
if opt.savedata then
    torch.save('training_data.net', trainset)
    torch.save('validation_data.net', validationset)
end
print(collectgarbage('count'))
