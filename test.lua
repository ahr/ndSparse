function eval(dataset, batch_size)
    local count = 0

    for i = 1, dataset.size, batch_size do
        -- local size = math.min(i + batch_size - 1, dataset.size) - i
        local inputs = {unpack(dataset.data, i, i+batch_size-1)}
        if opt.linear then
            inputs = toDenseTensor(inputs, 5013)
            if opt.cuda then
                input_tensor = input_tensor:copy(inputs)
            end
        end
        local targets = dataset.labels[{{i,i+batch_size-1}}]:long()
        if opt.cuda then
            outputs = model:forward(input_tensor)
        else
            outputs = model:forward(inputs)
        end
        local _, indices = torch.max(outputs, 2)
        indices = indices:long()
        local guessed_right = indices:eq(targets):sum()
        count = count + guessed_right
    end

    return count / dataset.size
end
