require 'src.Sparsed'
require 'nn'
require 'optim'
require 'image'
require 'cutorch'
require 'cunn'
mnist = require 'mnist'


fullset = mnist.traindataset()
testset = mnist.testdataset()

print(torch.type(fullset.data))

print(fullset.label[1])

trainset = {
    size = 50000,
    data = fullset.data[{{1,50000}}]:double(),
    reshaped = torch.DoubleTensor(50000, 28*28),
    sparsed = {},
    label = fullset.label[{{1,50000}}]
}

validationset = {
    size = 10000,
    data = fullset.data[{{50001,60000}}]:double(),
    reshaped = torch.DoubleTensor(10000, 28*28),
    sparsed = {},
    label = fullset.label[{{50001,60000}}]
}

newtestset = {
    size = 100,
    data = testset.data[{{1, 100}}]:double(),
    reshaped = torch.DoubleTensor(100, 28*28),
    sparsed = {},
    label = testset.label[{{1, 100}}]
}

model = nn.Sequential()
model:add(nn.Linear(28*28, 30))
model:add(nn.Tanh())
model:add(nn.Linear(30, 10))
model:add(nn.LogSoftMax())

model:cuda()
-- saving parameters of first layer to use in Sparsed
-- param, _ = model:get(1):parameters()
-- param[1][1][1] = 0.002
-- torch.save('weights1.net', param[1])
-- torch.save('biases1.net', param[2])
-- -- print(model:get(1):parameters()[1][1])
-- param1, _ = model:get(3):parameters()
-- param1[1][1][1] = 0.003
-- torch.save('weights2.net', param1[1])
-- torch.save('biases2.net', param1[2])
criterion = nn.ClassNLLCriterion()
criterion:cuda()
-- input and testset preparation


trainset.reshaped = nn.Reshape(28*28):forward(trainset.data)

validationset.reshaped = nn.Reshape(28*28):forward(validationset.data)

newtestset.reshaped = nn.Reshape(28*28):forward(newtestset.data)
-- print(trainset.reshaped[1])
-- print(validationset.reshaped[1])
-- image.display{image=validationset.data[96]}

sgd_params = {
   learningRate = 1e-2,
   learningRateDecay = 1e-4,
   weightDecay = 1e-3,
   momentum = 1e-4
}

x, dl_dx = model:getParameters()

function step(batch_size)
    local current_loss = 0
    local count = 0
    batch_size = batch_size or 200

    for t = 1, trainset.size, batch_size do
        -- setup inputs and targets for this mini-batch
        local size = math.min(t + batch_size - 1, trainset.size) - t
        local inputs = torch.Tensor(size, 28*28)
        local targets = torch.Tensor(size)
        for i = 1, size do
            inputs[i] = trainset.reshaped[t+i]
            targets[i] = trainset.label[t+i]
        end
        targets:add(1)

        -- if t < 2 then
        --     print(model:forward(inputs)[1])
        --     image.display(nn.Reshape(28, 28):forward(inputs[1]))
        --     image.display(nn.Reshape(28, 28):forward(inputs[2]))
        --     image.display(nn.Reshape(28, 28):forward(inputs[3]))
        --     image.display(nn.Reshape(28, 28):forward(inputs[4]))
        --     print(targets[1])
        --     print(targets[2])
        --     print(targets[3])
        --     print(targets[4])

        -- end
        local feval = function(x_new)
            -- reset data
            if x ~= x_new then x:copy(x_new) end
            dl_dx:zero()

            -- perform mini-batch gradient descent
            local loss = criterion:forward(model:forward(inputs), targets)
            model:backward(inputs, criterion:backward(model.output, targets))

            return loss, dl_dx
        end

        _, fs = optim.sgd(feval, x, sgd_params)
        -- fs is a table containing value of the loss function
        count = count + 1
        current_loss = current_loss + fs[1]
    end
    -- normalize loss
    return current_loss / count
end

function eval(dataset, batch_size)
    local count = 0
    batch_size = batch_size or 200

    for i = 1, dataset.size, batch_size do
        local size = math.min(i + batch_size - 1, dataset.size) - i
        local inputs = dataset.reshaped[{{i,i+size-1}}]
        local targets = dataset.label[{{i,i+size-1}}]:long()
        local outputs = model:forward(inputs)
        local _, indices = torch.max(outputs, 2)
        indices:add(-1)
        local guessed_right = indices:eq(targets):sum()
        count = count + guessed_right
    end

    return count / dataset.size
end

max_iters = 30

do
    print("Starting Training")
    local last_accuracy = 0
    local decreasing = 0
    local threshold = 1 -- number of deacreasing epochs allowed
    for i = 1,max_iters do
        local loss = step()
        print(string.format('Epoch: %d Current loss: %4f', i, loss))
        local accuracy = eval(validationset)
        print(string.format('Accuracy on the validation set: %4f', accuracy))
        if accuracy < last_accuracy then
            if decreasing > threshold then break end
            decreasing = decreasing + 1
        else
            decreasing = 0
        end
        last_accuracy = accuracy
    end
end

-- test on test set
test_accuracy = eval(newtestset)
print(string.format('Accuracy on the test set: %4f', test_accuracy))

-- save model
paths = require 'paths'
filename = paths.concat(paths.cwd(), 'modellinear.net')
torch.save(filename, model)
