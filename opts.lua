local M = { }

function M.parse(arg)
    local cmd = torch.CmdLine()
    cmd:text()
    cmd:text('Retail Dataset Mining')
    cmd:text()
    cmd:text('Options:')
    ------------ General options --------------------

    cmd:option('-cache', './models', 'subdirectory in which to save/log experiments')
    cmd:option('-loaddata', false, 'Run from cahced data [Default = false]')
    cmd:option('-savedata', false, 'Save data cache [Default = false]')
    cmd:option('-data', '../data/', 'Home of dataset')
    cmd:option('-manualSeed', 2, 'Manually set RNG seed')
    cmd:option('-cuda', false, 'Default No GPU')
    cmd:option('-nGPU', 1, 'Number of GPUs to use by default')
    cmd:option('-backend', 'nn', 'Options: cudnn | nn')
    ------------- Data options ------------------------
    -- cmd:option('-nDonkeys',        2, 'number of donkeys to initialize (data loading threads)')
    cmd:option('-nTrain', 100000, 'number of training examples [0 = All]')
    cmd:option('-nClasses', 8, 'number of classes in the dataset')
    ------------- Training options --------------------
    cmd:option('-nEpochs',  30, 'Number of total epochs to run')
    cmd:option('-epochSize', 100000, 'Number of batches per epoch')
    cmd:option('-epochNumber', 1, 'Manual epoch number (useful on restarts)')
    cmd:option('-batchSize', 1000,   'mini-batch size (1 = pure stochastic)')
    ---------- Optimization options ----------------------
    cmd:option('-LR', 0.1, 'learning rate; if set, overrides default LR/WD recipe')
    cmd:option('-momentum', 0.01,  'momentum')
    cmd:option('-weightDecay', 0.001, 'weight decay')
    ---------- Model options ----------------------------------
    cmd:option('-linear', false, 'Use Linear Layer instead of Sparse [False by Default]')
    cmd:option('-activation', 'Tanh', 'transfer function like ReLU, Tanh, Sigmoid')

    cmd:text()

    local opt = cmd:parse(arg or {})
    opt.save = paths.concat(opt.cache, '' .. os.date():gsub(' ',''))
    return opt
end

return M
