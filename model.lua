require 'nn'
require 'optim'


-- Create Model --
model = nn.Sequential()
if opt.linear then
    model:add(nn.Linear(5013,1000))
else
    model:add(nn.SparseLinear(5013, 1000, true))
    if opt.cuda then
      model:add(nn.Copy('torch.FloatTensor', 'torch.CudaTensor'))
    end
end
model:add(nn[opt.activation]())
model:add(nn.Linear(1000, 200))
model:add(nn[opt.activation]())
model:add(nn.Linear(200, 30))
model:add(nn[opt.activation]())
model:add(nn.Linear(30, nClasses))
model:add(nn.LogSoftMax())




-- Create Criterian --
criterion = nn.ClassNLLCriterion()

print('=> Model')
print(model)

print('=> Criterion')
print(criterion)

-- Convert model to CUDA --
if opt.cuda then
    -- require 'cunn'
    local counter = 2
    print('==> Converting model to CUDA')
    if opt.linear then counter = 1 end
    for i = counter, model:size() do
      model:get(i):cuda()
    end
    -- only works for linear
    if opt.linear then
      if opt.backend == 'cudnn' then
          require 'cudnn'
          cudnn.convert(model, cudnn)
      elseif opt.backend ~= 'nn' then
          error'Unsupported backend'
      end
    end
    criterion:cuda()
end
collectgarbage()
